﻿using System;

namespace BowlingGame
{
    public class Game
    {
      
        int[] pinFalls = new int[21];
        int rollCounter;
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        public void Roll(int pins)
        {
            pinFalls[rollCounter] += pins;
            rollCounter++;
        }

        public bool IsStrike(int i)
        {
            return pinFalls[i] == 10;
        }
        public bool IsSpare(int i)
        {
            return pinFalls[i] + pinFalls[i + 1] == 10;
        }

        public int StrikeBnous(int i)
        {
            return 10 + pinFalls[i + 1] + pinFalls[i + 2];
        }

        public int SpareBnous(int i)
        {
            return 10 + pinFalls[i + 2];
        }
        public int Score()
        {
            int score = 0;
            int i = 0;
            for (int frame = 0; frame < 10; frame++)
            {
                if(IsStrike(i))
                {
                    score += StrikeBnous(i);
                    i += 1;
                }
                else if (IsSpare(i))
                {
                    score += SpareBnous(i);
                    i += 2;
                }
                else
                {
                    score += pinFalls[i] + pinFalls[i + 1];
                    i += 2;
                }
            }
            return score;
        }
    }
}
