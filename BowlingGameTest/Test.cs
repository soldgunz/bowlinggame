﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp]
        public void SetUpGame()
        {
            game = new Game();
        }
        public void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }

        }
        [Test]
        public void RollGutter()
        {
            RollMany(20, 0);

            Assert.That(game.Score, Is.EqualTo(0));
        }
        [Test]
        public void RollOnes()
        {

            RollMany(20, 1);

            Assert.That(game.Score, Is.EqualTo(20));
        }
        [Test]
        public void RollFirstFrameSpare()
        {
            game.Roll(5);
            game.Roll(5);
            RollMany(18, 1);
            Assert.That(game.Score, Is.EqualTo(29));

        }
        [Test]
        public void RollFirstFrameStrike()
        {
            game.Roll(10);
            RollMany(19, 1);
            Assert.That(game.Score, Is.EqualTo(30));

        }
        [Test]
        public void PerfectGame()
        {
            RollMany(12, 10);
            Assert.That(game.Score, Is.EqualTo(300));

        }
        [Test]
        public void ALLSpare()
        {
            RollMany(21, 5);
            Assert.That(game.Score, Is.EqualTo(150));

        }
        [Test]
        public void FirstandSecondFrameSpare()
        {
            game.Roll(9);game.Roll(1);
            game.Roll(5); game.Roll(5);
            RollMany(16, 1);
            Assert.That(game.Score, Is.EqualTo(42));

        }
        [Test]
        public void TypicalGame()
        {
            game.Roll(10);
            game.Roll(9); game.Roll(1);
            game.Roll(5); game.Roll(5);
            game.Roll(7); game.Roll(2);
            game.Roll(10);
            game.Roll(10);
            game.Roll(10);
            game.Roll(9); game.Roll(0);
            game.Roll(8); game.Roll(2);
            game.Roll(9); game.Roll(1); game.Roll(10);

            Assert.That(game.Score, Is.EqualTo(187));

        }

    }
}
